using System;
namespace Temporal
{
    /// <summary>
    /// Classe per al objecte Data
    /// </summary>
    public class Data
    {
        /// <summary>
        /// Enter que representa el dia
        /// </summary>
        private int _dia;
        /// <summary>
        /// Enter que representa el mes
        /// </summary>
        private int _mes;
        /// <summary>
        /// Enter que representa l'any
        /// </summary>
        private int _any;
        /// <summary>
        /// Constructor agafant totes les variables per al objecte
        /// </summary>
        /// <param name="dia">Dia a posar a la data</param>
        /// <param name="mes">Mes a posar a la data</param>
        /// <param name="any">Any a posar a la data</param>
        public Data(int dia,int mes,int any)
        {
            if (Revisio(dia, mes, any))
            {
                _dia = dia;
                _mes = mes;
                _any = any;
            }
            else
            {
                _dia = 1;
                _mes = 1;
                _any = 1980;
            }
        }
        /// <summary>
        /// Constructor basat en clonació d'una altre data
        /// </summary>
        /// <param name="obj">Data la qual clonar</param>
        public Data(Data obj)
        {
            _dia = obj.GetDia();
            _mes = obj.GetMes();
            _any = obj.GetAny();
        }
        /// <summary>
        /// Constructor amb valors per defecte en cas de que estigui buit els paràmetres
        /// </summary>
        public Data()
        {
            _dia = 1;
            _mes = 1;
            _any = 1980;
        }
        /// <summary>
        /// Mètode que reuneix tots els mètodes per a revisar una data correctament
        /// </summary>
        /// <param name="dia">Dia el qual revisar</param>
        /// <param name="mes">Mes el qual revisar</param>
        /// <param name="any">Any el qual revisar</param>
        /// <returns></returns>
        private bool Revisio(int dia, int mes, int any)
        {
            if(AnyCheck(any)&&MesCheck(mes))
                if (DiaCheck(dia, mes, any))
                    return true;
            return false;
        }
        /// <summary>
        /// Variable de sortida de dades en format string
        /// </summary>
        /// <returns>Retorna la data en string</returns>
        public override string ToString()
        {
            return $"{_dia}/{_mes}/{_any}";
        }
        /// <summary>
        /// Mètode per a revisar el mes
        /// </summary>
        /// <param name="mes">Mes a revisar</param>
        /// <returns>Dona true si el més és correcte, fals en cas contrari</returns>
        private bool MesCheck(int mes)
        {
            return (mes > 0 && mes < 13);
        }
        /// <summary>
        /// Mètode per a revisar el dia
        /// </summary>
        /// <param name="day">Dia a revisar</param>
        /// <param name="mes">Mes a revisar</param>
        /// <param name="any">Any a revisar</param>
        /// <returns>Retorna true en cas de que sigui correcte, fals en cas contrari</returns>
        private bool DiaCheck(int day, int mes, int any)
        {
            return (day > 0 && day <= DiesMaxims(mes, any));
        }

        public int DiesMaxims(int mes,int any)
        {
            switch (mes)
            {
                case 1:
                    return (31);
                case 2:
                    if (any % 4 == 0 && any % 100 != 0)
                    {
                        return 29;
                    }
                    else return 28;
                case 3:
                    return 31;
                case 4:
                    return 30;
                case 5:
                    return 31;
                case 6:
                    return 30;
                case 7:
                    return 31;
                case 8:
                    return 31;
                case 9:
                    return 30;
                case 10:
                    return 31;
                case 11:
                    return 30;
                case 12:
                    return 31;
            }

            return 0;
        }
        /// <summary>
        /// Mètode per a revisar l'any
        /// </summary>
        /// <param name="any">Any a revisar</param>
        /// <returns>Dona true si l'any és correcte, fals en cas contrari</returns>
        private bool AnyCheck(int any)
        {
            return (any > -1);
        }
        /// <summary>
        /// Mètode per obtenir dia
        /// </summary>
        /// <returns>Ens retorna la variable dia</returns>
        public int GetDia()
        {
            return _dia;
        }
        /// <summary>
        /// Mètode per obtenir mes
        /// </summary>
        /// <returns>Ens retorna la variable mes</returns>
        public int GetMes()
        {
            return _mes;
        }
        /// <summary>
        /// Mètode per obtenir any
        /// </summary>
        /// <returns>Ens retorna la variable any</returns>
        public int GetAny()
        {
            return _any;
        }
        /// <summary>
        /// Mètode per a modificar l'any
        /// </summary>
        /// <param name="any">Any a posar</param>
        public void SetAny(int any)
        {
            if (AnyCheck(any) && DiaCheck(_dia, _mes, any)) _any = any;
        }
        /// <summary>
        /// Mètode per a modificar el més
        /// </summary>
        /// <param name="mes">Mes a posar</param>
        public void SetMes(int mes)
        {
            if (MesCheck(mes) && DiaCheck(_dia, mes, _any)) _mes = mes;
        }
        /// <summary>
        /// Mètode per a modificar el dia
        /// </summary>
        /// <param name="dia">Dia a posar</param>
        public void SetDia(int dia)
        {
            if (DiaCheck(dia, _mes, _any)) _dia = dia;
        }

        /// <summary>
        /// Mètode per a comparar si dos objectes són iguals
        /// </summary>
        /// <param name="obj">Objecte a comparar</param>
        /// <returns>Si el codi dels objectes i l'objecte en concret son el mateix, es true, sino, fals.</returns>
        public override bool Equals(Object obj)
        {
            if (obj == this) return true;
            if ((obj == null) || GetType() != obj.GetType()) return false;
            Data p = (Data) obj;
            return (_any == p.GetAny()&&_mes==p.GetMes()&&_dia==p.GetDia());
        }
        /// <summary>
        /// Mètode per a mostrar el codi hash de l'objecte
        /// </summary>
        /// <returns>Retorna int amb el codi</returns>
        public override int GetHashCode()
        {
            return GetDia()^GetMes()*GetAny();
        }
        /// <summary>
        /// Mètode per a restar o sumar a la data seleccionada.
        /// </summary>
        /// <param name="numero">Numero de dies a restar.</param>
        public void SumaEnter(int numero)
        {
                if(numero>0) for (int i = numero; i >= 1; i--)
                {
                    _dia++;
                    if (!Revisio(_dia, _mes, _any))
                    {
                        _dia = 1;
                        _mes++;
                        if (!MesCheck(_mes))
                        {
                            _any++;
                            _mes = 1;
                        }
                    }
                }
                if(numero<0) for (int i = numero; i <= -1; i++)
                {
                    _dia--;
                    if (!Revisio(_dia, _mes, _any))
                    {
                        _mes--;
                        _dia = DiesMaxims(_mes,_any);
                        if (!MesCheck(_mes))
                        {
                            _any--;
                            _mes = 12;
                            _dia = 31;
                        }
                    }
                }
        }
        /// <summary>
        /// Mètode per a calcular diferència de dies
        /// </summary>
        /// <param name="obj">Data la qual comparar</param>
        /// <returns>Retorna els dies que les separen</returns>
        public int DiesEntreDates(Data obj)
        {
            int numdata1=0;
            int numdata2=0;
            for (int j = obj.GetMes(); j >= 1; j--)
            {
                numdata1 += DiesMaxims(j, obj.GetAny());
            }
            for (int i = obj.GetAny()-1; i >= 1; i--)
            {
                for (int j = 12; j >= 1; j--)
                {
                    numdata1 += DiesMaxims(j, i);
                }
                
            }
            numdata1 -= DiesMaxims(obj.GetMes(), obj.GetAny()) - obj.GetDia();
            for (int j = GetMes(); j >= 1; j--)
            {
                numdata2 += DiesMaxims(j, GetAny());
            }
            for (int i = GetAny()-1; i >= 1; i--)
            {
                for (int j = 12; j >= 1; j--)
                {
                    numdata2 += DiesMaxims(j, i);
                }
                
            }
            numdata2 -= DiesMaxims(GetMes(), GetAny()) - GetDia();
            if (numdata1 > numdata2) return numdata1 - numdata2;
            return numdata2 - numdata1;
        }
    }
}