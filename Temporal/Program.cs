﻿using System;

namespace Temporal
{
    /// <summary>
    /// Classe que reuneix els mètodes de comprovació de Data.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Mètode d'inicialització que fa les comprovacions a data.
        /// </summary>
        public static void Main()
        {
            Data hola = new Data(-2, -8, -256);
            Console.WriteLine("Introduim dades erronies a la data i printem el que tingui: "+hola);
            hola.SetMes(3);
            Console.WriteLine("Modifiquem el més i revisem la seva integritat: "+hola);
            Data hola2 = new Data();
            Console.WriteLine("Generem una nova data amb constructor buit i les comparem, resultat: "+hola.Equals(hola2)+" , no son iguals.");
            Data hola3 = new Data(2, 3, 2003);
            Data hola4 = new Data(2, 4, 2003);
            Console.WriteLine("Mirem diferència de dies entre dues dates: "+hola3.DiesEntreDates(hola4));
            Console.WriteLine("Dates Extretes:");
            Console.WriteLine(hola3);
            Console.WriteLine(hola4);
            Console.WriteLine("Fem una resta de 30 dies a la data d'abril i mirem el resultat:");
            hola4.SumaEnter(-30);
            Console.WriteLine(hola4);
        }
    }
}