namespace P4M03UF4PolEgea
{
    public class Algorismes
    {
        public static void Ordenar(IOrdenable[] obj, int noElements)
        {
            for (int x = 0; x < noElements; x++)
            {
               for (int i = 0; i < noElements - 1; i++) {
                    int seguent = i + 1;
                    if (obj[i].Comparar(obj[seguent])>=1)
                    { 
                        (obj[i], obj[seguent]) = (obj[seguent], obj[i]);
                    }
               }
            } 
        }
    }
}