using System;
using System.Drawing;

namespace P4M03UF4PolEgea
{
    /// <summary>
    /// Classe filla de FiguraGeometrica3
    /// </summary>
    public class Cercle3:FiguraGeometrica3,IOrdenable
    {
        /// <summary>
        /// Variable que representa el radi de la circumferència
        /// </summary>
        private double _radi;
        /// <summary>
        /// Mètode per a l'extracció de dades per consola.
        /// </summary>
        /// <returns>Retorna un string generat per en cas d'extreure totes les dades.</returns>
        public override string ToString()
        {
            return $"Codi: {Codi}"+"\n"+$"Nom: {Nom}"+"\n"+$"Color: {Color}"+$"Radi: {_radi}"+"\n"+$"Perimetre: {Perimetre()}"+"\n"+$"Area: {Area()}"+"\n";
        }

        public int Comparar(IOrdenable x)
        {
            if (((Cercle3) x).Area() > Area()) return 1;
            if (((Cercle3) x).Area() > Area()) return 0;
            return -1;
        }

        /// <summary>
        /// Constructor de cercle el cual omplim totes les dades
        /// </summary>
        /// <param name="radi">Radi que volem que tingui l'objecte</param>
        /// <param name="codi">Codi que volem que tingui l'objecte</param>
        /// <param name="nom">Nom que volem que tingui l'objecte</param>
        /// <param name="color">Color que Volem que tingui l'objecte</param>
        public Cercle3(double radi,int codi,string nom,Color color): base(codi,nom, color)
        {
            _radi = radi;
        }
        /// <summary>
        /// Constructor utilitzat per a posar valors per defecte
        /// </summary>
        public Cercle3()
        {
            _radi = 1;
            Codi = 0;
            Nom="Undefined";
            Color=Color.Black;
        }
        /// <summary>
        /// Constructor utilitzat quan volem replicar un objecte en una nova adreça de memòria.
        /// </summary>
        /// <param name="lol">Objecte de la mateixa classe</param>
        public Cercle3(Cercle3 lol)
        {
            _radi = lol.GetRadi();
            Codi = lol.GetCodi();
            Nom = lol.GetNom();
            Color = lol.GetColor();
        }
        /// <summary>
        /// Mètode per a mostrar el radi
        /// </summary>
        /// <returns>Retorna el radi</returns>
        public double GetRadi()
        {
            return _radi;
        }
        /// <summary>
        /// Mètode per a modificar el radi
        /// </summary>
        /// <param name="radi">Radi a modificar</param>
        public void SetRadi(double radi)
        {
            _radi = radi;
        }
        /// <summary>
        /// Mètode per a calcular el perímetre
        /// </summary>
        /// <returns>Retorna el perímetre calculat</returns>
        public double Perimetre()
        {
            return 2*Math.PI*_radi;
        }
        /// <summary>
        /// Mètode per a calcular l'àrea
        /// </summary>
        /// <returns>Retorna l'àrea calculada</returns>
        public double Area()
        {
            return Math.PI*Math.Pow(_radi,2);
        }
        /// <summary>
        /// Mètode per a comparar si dos objectes són iguals
        /// </summary>
        /// <param name="obj">Objecte a comparar</param>
        /// <returns>Si el codi dels objectes i l'objecte en concret son el mateix, es true, sino, fals.</returns>
        public override bool Equals(Object obj)
        {
            if (obj == this) return true;
            if ((obj == null) || GetType() != obj.GetType()) return false; 
            Cercle3 p = (Cercle3) obj;
            return (Codi == p.GetCodi());
        }
        /// <summary>
        /// Mètode per a mostrar el codi hash de l'objecte
        /// </summary>
        /// <returns>Retorna int amb el codi</returns>
        public override int GetHashCode()
        {
            return GetNom().GetHashCode()^GetCodi()^GetColor().GetHashCode();
        }
    }
}