using System;

namespace ProvaUf4M03PolEgeaP2
{
    /// <summary>
    /// Classe abstracte Fitxa, pare dels objectes creats..
    /// </summary>
    public class Fitxa:IOrdenable
    {
        /// <summary>
        /// Referència de la fitxa.
        /// </summary>
        protected string Referencia;
        /// <summary>
        /// Títol de la fitxa.
        /// </summary>
        protected string Titol;
        /// <summary>
        /// Constructor Utilitzat per a posar valors donats per l'usuari.
        /// </summary>
        /// <param name="referencia">Referencia introduida per l'usuari.</param>
        /// <param name="titol">Titol introduit per l'usuari</param>
        public Fitxa(string referencia, string titol)
        {
            Referencia = referencia;
            Titol = titol;
        }
        /// <summary>
        /// Constructor utilitzat per a posar valors per defecte
        /// </summary>
        public Fitxa()
        {
            Referencia = "Undefined";
            Titol = "No title";
        }
        /// <summary>
        /// Constructor utilitzat per a copiar un objecte del mateix tipus.
        /// </summary>
        public Fitxa(Fitxa copia)
        {
            Referencia = copia.GetReferencia();
            Titol = copia.GetTitol();
        }
        /// <summary>
        /// Mètode que retorna la Referencia de la fitxa.
        /// </summary>
        /// <returns>Retorna la referencia de la fitxa.</returns>
        public string GetReferencia()
        {
            return Referencia;
        }
        /// <summary>
        /// Mètode que retorna el Títol de la fitxa.
        /// </summary>
        /// <returns>Retorna Titol de la fitxa.</returns>
        public string GetTitol()
        {
            return Titol;
        }
        /// <summary>
        /// Mètode que modifica Referencia de la fitxa amb valor donat per l'usuari.
        /// </summary>
        /// <param name="referencia">Referencia de la fitxa intoduida per l'usuari.</param>
        public void SetReferencia(string referencia)
        {
            Referencia=referencia;
        }
        /// <summary>
        /// Mètode que modifica Titol de la fitxa amb valor donat per l'usuari.
        /// </summary>
        /// <param name="titol">Titol de la fitxa intoduida per l'usuari.</param>
        public void SetTitol(string titol)
        {
            Titol=titol;
        }
        /// <summary>
        /// Mètode de sortida de dades en format de string de l'objecte.
        /// </summary>
        /// <returns>Retorna un string amb dades de l'objecte.</returns>
        public override string ToString()
        {
            return $"{Referencia}\t{Titol}";
        }
        /// <summary>
        /// Mètode per a comparar títols
        /// </summary>
        /// <param name="x">Objecte a comparar</param>
        /// <returns>Retorna 1 si es més gran, 0 si és igual, i -1 si és més petit.</returns>
        public int Comparar(IOrdenable x)
        {
            if (((Fitxa) x).GetTitol().Length > Titol.Length) return 1;
            if (((Fitxa) x).GetTitol().Length == Titol.Length) return 0;
            return -1;
        }

        /// <summary>
        /// Mètode per a comparar si dos objectes són iguals
        /// </summary>
        /// <param name="obj">Objecte a comparar</param>
        /// <returns>Si el codi dels objectes i l'objecte en concret son el mateix, es true, sino, fals.</returns>
        public override bool Equals(Object obj)
        {
            if (obj == this) return true;
            if ((obj == null) || GetType() != obj.GetType()) return false; 
            Fitxa p = (Fitxa) obj;
            return (Referencia == p.GetReferencia());
        }
        /// <summary>
        /// Mètode per a mostrar el codi hash de l'objecte
        /// </summary>
        /// <returns>Retorna int amb el codi</returns>
        public override int GetHashCode()
        {
            return GetReferencia().GetHashCode()^GetTitol().GetHashCode();
        }
    }
}