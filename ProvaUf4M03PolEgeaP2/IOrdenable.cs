namespace ProvaUf4M03PolEgeaP2
{
    /// <summary>
    /// Interfaç per a poder comparar objectes.
    /// </summary>
    public interface IOrdenable
    {
        /// <summary>
        /// Mètode detallat als objectes per a comparar.
        /// </summary>
        /// <param name="x">objecte a comparar.</param>
        /// <returns>Especificat als objectes.</returns>
        int Comparar(IOrdenable x);
    }
}