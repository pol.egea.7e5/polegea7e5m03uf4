namespace ProvaUf4M03PolEgeaP2
{
    /// <summary>
    /// Classe per al metode d'ordenació d'un vector.
    /// </summary>
    public class Algorismes
    {
        /// <summary>
        /// Mètode que permet ordenar un vector.
        /// </summary>
        /// <param name="obj">Vector a ordenar.</param>
        /// <param name="noElements">Elements que te el vector.</param>
        public static void Ordenar(IOrdenable[] obj, int noElements)
        {
            for (int x = 0; x < noElements; x++)
            {
               for (int i = 0; i < noElements - 1; i++) {
                    int seguent = i + 1;
                    if (obj[i].Comparar(obj[seguent])==1)
                    { 
                        (obj[i], obj[seguent]) = (obj[seguent], obj[i]);
                    }
               }
            } 
        }
    }
}