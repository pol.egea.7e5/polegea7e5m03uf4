using System;

namespace ProvaUf4M03PolEgeaP2
{
    /// <summary>
    /// Classe per al objecte Volum.
    /// </summary>
    public class Volum:Obra
    {
        /// <summary>
        /// Numero de l'obra introduit per l'usuari.
        /// </summary>
        private short _nro;
        /// <summary>
        /// Constructor Utilitzat per a posar valors donats per l'usuari.
        /// </summary>
        /// <param name="autor">Any introduit per l'usuari.</param>
        /// <param name="nro">Numero introduit per l'usuari.</param>
        /// <param name="nrePags">Numero de pagines introduit per l'usuari.</param>
        /// <param name="referencia">Referencia introduida per l'usuari.</param>
        /// <param name="titol">Titol introduit per l'usuari</param>
        public Volum(short nro,string autor,short nrePags,string referencia, string titol): base(autor,nrePags, referencia, titol)
        {
            _nro = nro;
        }
        /// <summary>
        /// Constructor utilitzat per a posar valors per defecte
        /// </summary>
        public Volum()
        {
            _nro = 0;
            Autor = "No Autor";
            NrePags = 0;
            Referencia="Undefined";
            Titol = "No Titol";
        }
        /// <summary>
        /// Constructor utilitzat per a copiar un objecte del mateix tipus.
        /// </summary>
        public Volum(Volum copia)
        {
            _nro = copia.GetNro();
            Autor = copia.GetAutor();
            NrePags = copia.GetNrePags();
            Referencia = copia.GetReferencia();
            Titol = copia.GetTitol();
        }
        /// <summary>
        /// Mètode que retorna el Numero de l'obra.
        /// </summary>
        /// <returns>Retorna Numero de l'obra.</returns>
        public short GetNro()
        {
            return _nro;
        }
        /// <summary>
        /// Mètode que modifica Numero de l'obra amb valor donat per l'usuari.
        /// </summary>
        /// <param name="nro">Numero de l'obra intoduit per l'usuari.</param>
        public void SetNro(short nro)
        {
            _nro = nro;
        }
        /// <summary>
        /// Mètode de sortida de dades en format de string de l'objecte.
        /// </summary>
        /// <returns>Retorna un string amb dades de l'objecte.</returns>
        public override string ToString()
        {
            return $"{Referencia}\t{Titol}\t{Autor}\t{NrePags}\t{_nro}";
        }
        /// <summary>
        /// Mètode per a comparar si dos objectes són iguals
        /// </summary>
        /// <param name="obj">Objecte a comparar</param>
        /// <returns>Si el codi dels objectes i l'objecte en concret son el mateix, es true, sino, fals.</returns>
        public override bool Equals(Object obj)
        {
            if (obj == this) return true;
            if ((obj == null) || GetType() != obj.GetType()) return false; 
            Volum p = (Volum) obj;
            return (Referencia == p.GetReferencia());
        }
        /// <summary>
        /// Mètode per a mostrar el codi hash de l'objecte
        /// </summary>
        /// <returns>Retorna int amb el codi</returns>
        public override int GetHashCode()
        {
            return GetReferencia().GetHashCode()^GetTitol().GetHashCode();
        }
    }
}