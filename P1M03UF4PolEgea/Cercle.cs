using System;
using System.Drawing;

namespace P1M03UF4PolEgea
{
    public class Cercle
    {
        private double _radi;
        protected int Codi;
        protected string Nom;
        protected Color Color;
        
        public override string ToString()
        {
            return $"Codi: {Codi}"+"\n"+$"Nom: {Nom}"+"\n"+$"Color: {Color}"+$"Radi: {_radi}"+"\n"+$"Perimetre: {Perimetre()}"+"\n"+$"Area: {Area()}"+"\n";
        }

        public Cercle(double radi,int codi,string nom,Color color)
        {
            _radi = radi;
            Codi = codi;
            Nom = nom;
            Color = color;
        }
        public Cercle()
        {
            _radi = 1;
            Codi = 0;
            Nom="Undefined";
            Color=Color.Black;
        }

        public Cercle(Cercle lol)
        {
            _radi = lol.GetRadi();
            Codi = lol.GetCodi();
            Nom = lol.GetNom();
            Color = lol.GetColor();
        }
        public double GetRadi()
        {
            return _radi;
        }
        public int GetCodi()
        {
            return Codi;
        }
        public string GetNom()
        {
            return Nom;
        }
        public Color GetColor()
        {
            return Color;
        }
        public void SetCodi(int codi)
        {
            Codi=codi;
        }
        public void SetNom(string nom)
        {
            Nom=nom;
        }
        public void SetColor(Color color)
        {
            Color=color;
        }
        public void SetRadi(double radi)
        {
            _radi = radi;
        }
        public double Perimetre()
        {
            return 2*Math.PI*_radi;
        }
        public double Area()
        {
            return Math.PI*Math.Pow(_radi,2);
        }
    }
}