using System.Drawing;

namespace P1M03UF4PolEgea
{
    /// <summary>
    /// Classe per al objecte figuresgeometriques.
    /// </summary>
    public class FiguraGeometrica
    {
        /// <summary>
        /// Codi de la figura.
        /// </summary>
        protected int Codi;
        /// <summary>
        /// Nom de la figura.
        /// </summary>
        protected string Nom;
        /// <summary>
        /// Color de la figura.
        /// </summary>
        protected Color Color;
        /// <summary>
        /// Mètode per a l'extracció de dades per consola.
        /// </summary>
        /// <returns>Retorna un string generat per en cas d'extreure totes les dades.</returns>
        public override string ToString()
        {
            return $"Codi: {Codi}"+"\n"+$"Nom: {Nom}"+"\n"+$"Color: {Color}";
        }

        public FiguraGeometrica(int codi,string nom,Color color)
        {
            Codi = codi;
            Nom = nom;
            Color = color;
        }

        public FiguraGeometrica()
        {
            Codi = 0;
            Nom="Undefined";
            Color=Color.Black;
        }

        public FiguraGeometrica(FiguraGeometrica lol)
        {
            Codi = lol.Codi;
            Nom = lol.Nom;
            Color = lol.Color;
        }

        public int GetCodi()
        {
            return Codi;
        }
        public string GetNom()
        {
            return Nom;
        }
        public Color GetColor()
        {
            return Color;
        }
        public void SetCodi(int codi)
        {
            Codi=codi;
        }
        public void SetNom(string nom)
        {
            Nom=nom;
        }
        public void SetColor(Color color)
        {
            Color=color;
        }
    }
}