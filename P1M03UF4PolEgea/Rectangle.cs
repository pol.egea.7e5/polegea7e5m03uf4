using System.Drawing;

namespace P1M03UF4PolEgea
{
    public class Rectangle
    {
        private double _base;
        private double _altura;
        protected int Codi;
        protected string Nom;
        protected Color Color;
        
        public override string ToString()
        {
            return $"Codi: {Codi}"+"\n"+$"Nom: {Nom}"+"\n"+$"Color: {Color}"+$"Base: {_base}"+"\n"+$"Altura: {_altura}"+"\n"+$"Perimetre: {Perimetre()}"+"\n"+$"Area: {Area()}"+"\n";
        }

        public Rectangle(double bbase,double altura,int codi,string nom,Color color)
        {
            _base = bbase;
            _altura = altura;
            Codi = codi;
            Nom = nom;
            Color = color;
        }
        public Rectangle()
        {
            _base = 1;
            _altura = 1;
            Codi = 0;
            Nom="Undefined";
            Color=Color.Black;
        }

        public Rectangle(Rectangle lol)
        {
            _base = lol.GetBase();
            _altura = lol.GetAltura();
            Codi = lol.GetCodi();
            Nom = lol.GetNom();
            Color = lol.GetColor();
        }

        public double GetBase()
        {
            return _base;
        }
        public double GetAltura()
        {
            return _altura;
        }
        public int GetCodi()
        {
            return Codi;
        }
        public string GetNom()
        {
            return Nom;
        }
        public Color GetColor()
        {
            return Color;
        }
        public void SetCodi(int codi)
        {
            Codi=codi;
        }
        public void SetNom(string nom)
        {
            Nom=nom;
        }
        public void SetColor(Color color)
        {
            Color=color;
        }
         public void SetBase(double bbase)
        {
            _base=bbase;
        }
        public void SetAltura(double altura)
        {
            _altura=altura;
        }
        public double Perimetre()
        {
            return _base * 4;
        }
        public double Area()
        {
            return _base * _altura;
        }
    }
}