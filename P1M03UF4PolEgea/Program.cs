﻿using System;
using System.Drawing;

namespace P1M03UF4PolEgea
{
    /// <summary>
    /// Classe per al main i la comprovació de mètodes.
    /// </summary>
    internal class ProvaFigures
    {
        /// <summary>
        /// Mètode que revisa cada una de les classes creades.
        /// </summary>
        public static void Main()
        {
            Rectangle nou = new Rectangle();
            Console.WriteLine(nou);
            Rectangle noucopia = new Rectangle(nou);
            noucopia.SetBase(noucopia.GetBase()*100);
            Console.WriteLine(noucopia);
            Rectangle cercol = new Rectangle(10, 100, 1090, "Lol", Color.Aquamarine);
            Console.WriteLine(cercol);
            Triangle nou2 = new Triangle();
            Console.WriteLine(nou2);
            Triangle noucopia2 = new Triangle(nou2);
            noucopia2.SetAltura(noucopia2.GetAltura()*100);
            Console.WriteLine(noucopia2);
            Triangle cercol2 = new Triangle(10, 100, 1090, "Lol", Color.Aquamarine);
            Console.WriteLine(cercol2);
            Cercle noucercle = new Cercle();
            Console.WriteLine(noucercle);
            Cercle noucercle2 = new Cercle(23, 2039, "Cercle", Color.Brown);
            Cercle copiacercle = new Cercle(noucercle2);
            noucercle2.SetColor(Color.Coral);
            Console.WriteLine(noucercle2);
            Console.WriteLine(copiacercle);
        }
    }
}