using System;

namespace ProvaUf4M03PolEgeaP1
{
    /// <summary>
    /// Classe per al objecte Obra.
    /// </summary>
    public class Obra:Fitxa
    {
        /// <summary>
        /// Autor de l'obra.
        /// </summary>
        protected string Autor;
        /// <summary>
        /// Numero de pagines de l'obra.
        /// </summary>
        protected short NrePags;
        /// <summary>
        /// Constructor Utilitzat per a posar valors donats per l'usuari.
        /// </summary>
        /// <param name="autor">Autor introduit per l'usuari.</param>
        /// <param name="nrePags">Numero de pagines introduit per l'usuari.</param>
        /// <param name="referencia">Referencia introduida per l'usuari.</param>
        /// <param name="titol">Titol introduit per l'usuari</param>
        public Obra(string autor,short nrePags,string referencia, string titol): base(referencia,titol)
        {
            Autor = autor;
            NrePags = nrePags;
        }
        /// <summary>
        /// Constructor utilitzat per a posar valors per defecte
        /// </summary>
        public Obra()
        {
            Autor = "No Autor";
            NrePags = 0;
            Referencia="Undefined";
            Titol = "No Titol";
        }
        /// <summary>
        /// Constructor utilitzat per a copiar un objecte del mateix tipus.
        /// </summary>
        public Obra(Obra copia)
        {
            Autor = copia.GetAutor();
            NrePags = copia.GetNrePags();
            Referencia = copia.GetReferencia();
            Titol = copia.GetTitol();
        }
        /// <summary>
        /// Mètode que retorna l'autor de l'obra.
        /// </summary>
        /// <returns>Retorna l'autor de l'obra.</returns>
        public string GetAutor()
        {
            return Autor;
        }
        /// <summary>
        /// Mètode que retorna el Numero de pàgines de l'obra.
        /// </summary>
        /// <returns>Retorna Numero de pàgines de l'obra.</returns>
        public short GetNrePags()
        {
            return NrePags;
        }
        /// <summary>
        /// Mètode que modifica Autor de l'obra amb valor donat per l'usuari.
        /// </summary>
        /// <param name="autor">Autor de l'obra intoduit per l'usuari.</param>
        public void SetAutor(string autor)
        {
            Autor = autor;
        }
        /// <summary>
        /// Mètode que modifica Numero de pàgines de l'obra amb valor donat per l'usuari.
        /// </summary>
        /// <param name="nrePags">Numero de pagines de l'obra intoduit per l'usuari.</param>
        public void SetNrePags(short nrePags)
        {
            NrePags = nrePags;
        }
        /// <summary>
        /// Mètode de sortida de dades en format de string de l'objecte.
        /// </summary>
        /// <returns>Retorna un string amb dades de l'objecte.</returns>
        public override string ToString()
        {
            return $"{Referencia}\t{Titol}\t{Autor}\t{NrePags}";
        }
        /// <summary>
        /// Mètode per a comparar si dos objectes són iguals
        /// </summary>
        /// <param name="obj">Objecte a comparar</param>
        /// <returns>Si el codi dels objectes i l'objecte en concret son el mateix, es true, sino, fals.</returns>
        public override bool Equals(Object obj)
        {
            if (obj == this) return true;
            if ((obj == null) || GetType() != obj.GetType()) return false; 
            Obra p = (Obra) obj;
            return (Referencia == p.GetReferencia());
        }
        /// <summary>
        /// Mètode per a mostrar el codi hash de l'objecte
        /// </summary>
        /// <returns>Retorna int amb el codi</returns>
        public override int GetHashCode()
        {
            return GetReferencia().GetHashCode()^GetTitol().GetHashCode();
        }
    }
}