using System;

namespace ProvaUf4M03PolEgeaP1
{
    /// <summary>
    /// Classe per al objecte Revista.
    /// </summary>
    public class Revista:Fitxa
    {
        /// <summary>
        /// Any de la revista.
        /// </summary>
        private short _any;
        /// <summary>
        /// Numero de la revista.
        /// </summary>
        private short _nro;
        /// <summary>
        /// Constructor Utilitzat per a posar valors donats per l'usuari.
        /// </summary>
        /// <param name="any">Any introduit per l'usuari.</param>
        /// <param name="nro">Numero introduit per l'usuari.</param>
        /// <param name="referencia">Referencia introduida per l'usuari.</param>
        /// <param name="titol">Titol introduit per l'usuari</param>
        public Revista(short any,short nro,string referencia, string titol): base(referencia,titol)
        {
            _any = any;
            _nro = nro;
        }
        /// <summary>
        /// Constructor utilitzat per a posar valors per defecte
        /// </summary>
        public Revista()
        {
            _any = 0;
            _nro = 0;
            Referencia="Undefined";
            Titol = "No Titol";
        }
        /// <summary>
        /// Constructor utilitzat per a copiar un objecte del mateix tipus.
        /// </summary>
        public Revista(Revista copia)
        {
            _any = copia.GetAny();
            _nro = copia.GetNro();
            Referencia = copia.GetReferencia();
            Titol = copia.GetTitol();
        }
        /// <summary>
        /// Mètode de sortida de l'any.
        /// </summary>
        /// <returns>Retorna Any.</returns>
        public short GetAny()
        {
            return _any;
        }
        /// <summary>
        /// Mètode de sortida del numero de la revista.
        /// </summary>
        /// <returns>Retorna Numero de la revista.</returns>
        public short GetNro()
        {
            return _nro;
        }
        /// <summary>
        /// Mètode que modifica Any amb valor donat per l'usuari.
        /// </summary>
        /// <param name="any">Any intoduit per l'usuari.</param>
        public void SetAny(short any)
        {
            _any = any;
        }
        /// <summary>
        /// Mètode que modifica el numero amb valor donat per l'usuari.
        /// </summary>
        /// <param name="nro">Numero intoduit per l'usuari.</param>
        public void SetNro(short nro)
        {
            _nro = nro;
        }
        /// <summary>
        /// Mètode de sortida de dades en format de string de l'objecte.
        /// </summary>
        /// <returns>Retorna un string amb dades de l'objecte.</returns>
        public override string ToString()
        {
            return $"{Referencia}\t{Titol}\t{_any}\t{_nro}";
        }
        /// <summary>
        /// Mètode per a comparar si dos objectes són iguals
        /// </summary>
        /// <param name="obj">Objecte a comparar</param>
        /// <returns>Si el codi dels objectes i l'objecte en concret son el mateix, es true, sino, fals.</returns>
        public override bool Equals(Object obj)
        {
            if (obj == this) return true;
            if ((obj == null) || GetType() != obj.GetType()) return false; 
            Revista p = (Revista) obj;
            return (Referencia == p.GetReferencia());
        }
        /// <summary>
        /// Mètode per a mostrar el codi hash de l'objecte
        /// </summary>
        /// <returns>Retorna int amb el codi</returns>
        public override int GetHashCode()
        {
            return GetReferencia().GetHashCode()^GetTitol().GetHashCode();
        }
    }
}