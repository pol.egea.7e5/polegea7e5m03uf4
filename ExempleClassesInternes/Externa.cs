using System;

namespace ExempleClassesInternes
{
    public class Externa
    {
        public void Metode1()
        {
            Console.WriteLine("Classe Externa");
        }
        public class Interna
        {
            public void Metode2()
            {
                Console.WriteLine("Classe Interna");
            }
        }
    }
}