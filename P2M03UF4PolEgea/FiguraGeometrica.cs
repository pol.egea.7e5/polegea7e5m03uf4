using System;
using System.Drawing;

namespace P2M03UF4PolEgea
{
    /// <summary>
    /// Classe per al objecte figuresgeometriques.
    /// </summary>
    public abstract class FiguraGeometrica
    {
        /// <summary>
        /// Codi de la figura.
        /// </summary>
        protected int Codi;
        /// <summary>
        /// Nom de la figura.
        /// </summary>
        protected string Nom;
        /// <summary>
        /// Color de la figura.
        /// </summary>
        protected Color Color;
        /// <summary>
        /// Mètode per a l'extracció de dades per consola.
        /// </summary>
        /// <returns>Retorna un string generat per en cas d'extreure totes les dades.</returns>
        public override string ToString()
        {
            return $"Codi: {Codi}"+"\n"+$"Nom: {Nom}"+"\n"+$"Color: {Color}";
        }
        /// <summary>
        /// Constructor de cercle el cual omplim totes les dades
        /// </summary>
        /// <param name="codi">Codi que volem que tingui l'objecte</param>
        /// <param name="nom">Nom que volem que tingui l'objecte</param>
        /// <param name="color">Color que Volem que tingui l'objecte</param>
        public FiguraGeometrica(int codi,string nom,Color color)
        {
            Codi = codi;
            Nom = nom;
            Color = color;
        }
        /// <summary>
        /// Constructor utilitzat per a posar valors per defecte
        /// </summary>
        public FiguraGeometrica()
        {
            Codi = 0;
            Nom="Undefined";
            Color=Color.Black;
        }
        /// <summary>
        /// Constructor utilitzat quan volem replicar un objecte en una nova adreça de memòria.
        /// </summary>
        /// <param name="obj">Objecte de la mateixa classe</param>
        public FiguraGeometrica(FiguraGeometrica obj)
        {
            Codi = obj.GetCodi();
            Nom = obj.GetNom();
            Color = obj.GetColor();
        }
        /// <summary>
        /// Mètode per a mostrar el codi
        /// </summary>
        /// <returns>retorna el codi</returns>
        public int GetCodi()
        {
            return Codi;
        }
        /// <summary>
        /// Mètode per a mostrar el nom
        /// </summary>
        /// <returns>retorna el nom</returns>
        public string GetNom()
        {
            return Nom;
        }
        /// <summary>
        /// Mètode per mostrar el color
        /// </summary>
        /// <returns>retorna el color</returns>
        public Color GetColor()
        {
            return Color;
        }
        /// <summary>
        /// Mètodi per a modificar el codi
        /// </summary>
        /// <param name="codi">Codi a utilitzar</param>
        public void SetCodi(int codi)
        {
            Codi=codi;
        }
        /// <summary>
        /// Mètode per a modificar el nom
        /// </summary>
        /// <param name="nom">Nom a utilitzar</param>
        public void SetNom(string nom)
        {
            Nom=nom;
        }
        /// <summary>
        /// Mètode per a modificar el color
        /// </summary>
        /// <param name="color">Color a utilitzar</param>
        public void SetColor(Color color)
        {
            Color=color;
        }
        /// <summary>
        /// Mètode per a comparar si dos objectes són iguals
        /// </summary>
        /// <param name="obj">Objecte a comparar</param>
        /// <returns>Si el codi dels objectes i l'objecte en concret son el mateix, es true, sino, fals.</returns>
        public override bool Equals(Object obj)
        {
            if (obj == this) return true;
            if ((obj == null) || GetType() != obj.GetType()) return false;
            FiguraGeometrica p = (FiguraGeometrica) obj;
            return (Codi == p.Codi);
        }
        /// <summary>
        /// Mètode per a mostrar el codi hash de l'objecte
        /// </summary>
        /// <returns>Retorna int amb el codi</returns>
        public override int GetHashCode()
        {
            return GetNom().GetHashCode()^GetCodi()^GetColor().GetHashCode();
        }
    }
}