﻿using System;

namespace P2M03UF4PolEgea
{
    /// <summary>
    /// Classe per al main i la comprovació de mètodes.
    /// </summary>
    internal class ProvaFigures
    {
        /// <summary>
        /// Mètode que revisa cada una de les classes creades.
        /// </summary>
        public static void Main()
        {
            ProvaRectangle();
            ProvaTriangle();
            ProvaCercle();
        }
        /// <summary>
        /// Mètode que revisa els mètodes del objecte Rectangle
        /// </summary>
        public static void ProvaRectangle()
        {
            Rectangle nou = new Rectangle();
            Rectangle nou2 = new Rectangle(nou);
            Console.WriteLine("Mirem si els dos rectangles generats son iguals, un està fet a partir de l'altre: "+nou.Equals(nou2));
            nou2.SetCodi(69);
            Console.WriteLine("Un cop canviat el codi, mirem si tornen a ser iguals: "+nou.Equals(nou2));
        }
        /// <summary>
        /// Mètode que revisa els mètodes del objecte Triangle
        /// </summary>
        public static void ProvaTriangle()
        {
            Triangle nou = new Triangle();
            Triangle nou2 = new Triangle(nou);
            Console.WriteLine("Mirem si els dos triangles generats son iguals, un està fet a partir de l'altre: "+nou.Equals(nou2));
            nou2.SetCodi(69);
            Console.WriteLine("Un cop canviat el codi, mirem si tornen a ser iguals: "+nou.Equals(nou2));
        }
        /// <summary>
        /// Mètode que revisa els mètodes del objecte Cercle
        /// </summary>
        public static void ProvaCercle()
        {
            Cercle nou = new Cercle();
            Cercle nou2 = new Cercle(nou);
            Console.WriteLine("Mirem si els dos cercles generats son iguals, un està fet a partir de l'altre: "+nou.Equals(nou2));
            nou2.SetCodi(69);
            Console.WriteLine("Un cop canviat el codi, mirem si tornen a ser iguals: "+nou.Equals(nou2));
        }
    }
}