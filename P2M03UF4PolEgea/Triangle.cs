using System;
using System.Drawing;

namespace P2M03UF4PolEgea
{
    /// <summary>
    /// Classe filla de FiguraGeometrica
    /// </summary>
    public class Triangle:FiguraGeometrica
    {
        /// <summary>
        /// Variable que representa la base del triangle
        /// </summary>
        private double _base;
        /// <summary>
        /// Variable que representa l'alçada del triangle
        /// </summary>
        private double _altura;
        /// <summary>
        /// Mètode per a l'extracció de dades per consola.
        /// </summary>
        /// <returns>Retorna un string generat per en cas d'extreure totes les dades.</returns>
        public override string ToString()
        {
            return $"Codi: {Codi}"+"\n"+$"Nom: {Nom}"+"\n"+$"Color: {Color}"+$"Base: {_base}"+"\n"+$"Altura: {_altura}"+"\n"+$"Perimetre: {Perimetre()}"+"\n"+$"Area: {Area()}"+"\n";
        }
        /// <summary>
        /// Constructor de cercle el cual omplim totes les dades
        /// </summary>
        /// <param name="bbase">Base que volem que tingui l'objecte</param>
        /// <param name="altura">Alçada que volem que tingui l'objecte</param>
        /// <param name="codi">Codi que volem que tingui l'objecte</param>
        /// <param name="nom">Nom que volem que tingui l'objecte</param>
        /// <param name="color">Color que Volem que tingui l'objecte</param>
        public Triangle(double bbase,double altura,int codi,string nom,Color color): base(codi,nom, color)
        {
            _base = bbase;
            _altura = altura;
        }
        /// <summary>
        /// Constructor utilitzat per a posar valors per defecte
        /// </summary>
        public Triangle()
        {
            _base = 1;
            _altura = 1;
            Codi = 0;
            Nom="Undefined";
            Color=Color.Black;
        }
        /// <summary>
        /// Constructor utilitzat quan volem replicar un objecte en una nova adreça de memòria.
        /// </summary>
        /// <param name="lol">Objecte de la mateixa classe</param>
        public Triangle(Triangle lol)
        {
            _base = lol.GetBase();
            _altura = lol.GetAltura();
            Codi = lol.GetCodi();
            Nom = lol.GetNom();
            Color = lol.GetColor();
        }

        /// <summary>
        /// Mètode per a mostrar la base
        /// </summary>
        /// <returns>Retorna la base</returns>
        public double GetBase()
        {
            return _base;
        }
        /// <summary>
        /// Mètode per a mostrar l'alçada
        /// </summary>
        /// <returns>Retorna l'alçada</returns>
        public double GetAltura()
        {
            return _altura;
        }
        /// <summary>
        /// Mètode per a modificar la base
        /// </summary>
        /// <param name="bbase">Base a posar</param>
        public void SetBase(double bbase)
        {
            _base=bbase;
        }
        /// <summary>
        /// Mètode per a modificar l'alçada
        /// </summary>
        /// <param name="altura">Alçada a ficar</param>
        public void SetAltura(double altura)
        {
            _altura=altura;
        }
        /// <summary>
        /// Mètode per a calcular el perímetre
        /// </summary>
        /// <returns>Retorna el perímetre calculat</returns>
        public double Perimetre()
        {
            return _base * 3;
        }
        /// <summary>
        /// Mètode per a calcular l'àrea
        /// </summary>
        /// <returns>Retorna l'àrea calculada</returns>
        public double Area()
        {
            return _base * _altura / 2;
        }
        /// <summary>
        /// Mètode per a comparar si dos objectes són iguals
        /// </summary>
        /// <param name="obj">Objecte a comparar</param>
        /// <returns>Si el codi dels objectes i l'objecte en concret son el mateix, es true, sino, fals.</returns>
        public override bool Equals(Object obj)
        {
            if (obj == this) return true;
            if ((obj == null) || GetType() != obj.GetType()) return false; 
            Triangle p = (Triangle) obj;
            return (Codi == p.GetCodi());
        }
        /// <summary>
        /// Mètode per a mostrar el codi hash de l'objecte
        /// </summary>
        /// <returns>Retorna int amb el codi</returns>
        public override int GetHashCode()
        {
            return GetNom().GetHashCode()^GetCodi()^GetColor().GetHashCode();
        }
    }
}