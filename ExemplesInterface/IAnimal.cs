namespace ExemplesInterface
{
    public interface IAnimal
    {
        string Descripcio();

        string Nom { get; set; }
    }
}