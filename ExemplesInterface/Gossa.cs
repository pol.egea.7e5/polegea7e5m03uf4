using System;

namespace ExemplesInterface
{
    public class Gossa:IAnimal,IComparable
    {
        private string _nom;

        public Gossa(string nom)
        {
            Nom = nom;
            _nom = nom;
        }

        public string Descripcio()
        {
            return "Hola, sóc una gossa i el meu nom és " + Nom;
        }

        public string Nom
        {
            get {return _nom;} set {_nom = value;}
        }

        public int CompareTo(object obj)
        {
            if (obj is IAnimal) return String.Compare(Nom,(obj as IAnimal)?.Nom,StringComparison.Ordinal);
            return 0;
        }
    }
}